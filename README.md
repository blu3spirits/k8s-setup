# K8s

## Dependencies
- Docker
- CRIO (Should just be installed upon kubeadm)

## Setting up kubeadm, kubelet, kubectl

```
apt-get update && apt-get install -y apt-transport-https curl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF
apt-get update
apt-get install -y kubelet kubeadm kubectl
apt-mark hold kubelet kubeadm kubectl
```

Restart kubelet

```
systemctl daemon-reload
systemctl restart kubelet
```

## Initialize kubeadm

```
kubeadm init --pod-network-cidr=10.0.0.0/8
```

## Adjust docker daemon

```
cat > /etc/docker/daemon.json <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF

systemctl daemon-reload
systemctl restart docker
```

## Cluster networking

### Calico

```
# USER configs
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

kubectl apply -f https://docs.projectcalico.org/v3.7/manifests/calico.yaml

# Confirm command success with
# watch kubectl get pods --all-namespaces
```

### Kube-flannel

```
kubectl apply -f https://github.com/coreos/flannel/blob/master/Documentation/kube-flannel.yml
```

### Helm

[Install Helm from here](https://helm.sh/docs/install/)


## Adding nodes to the cluster

Pay careful attention to the output of the init call because you'll need the token and ca-cert hash. It should look something like this:

```
kubeadm join $IP --token $TOKEN \
    --discovery-token-ca-cert-hash $HASH
```
